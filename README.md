# Loti

My Drupal 8 base theme with top notch HTML5/CSS gems :)

## Generate stylsheets

Install a SASS compiler

```
npm install
```

and generate the stylesheets:
```
npm run watch
```
